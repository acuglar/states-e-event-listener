import React from 'react';
import './App.css';
import NumberList from './components/NumberList';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberList: []
    }
  }

  addNumber = () => {
    const { numberList } = this.state;
    numberList.length !== 0 ? numberList.push(numberList[numberList.length - 1] + 10) : numberList.push(10)
    this.setState({ numberList: numberList })
  }
  render() {
    return (
      <div className="App">
        <button onClick={this.addNumber}>Adicionar Número</button>
        <NumberList list={this.state.numberList} />
      </div>
    );
  }
}

export default App;
