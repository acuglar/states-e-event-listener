import { Component } from 'react';
import Number from './Number';

class NumberList extends Component {
  render() {
    return (
      <div>
        {this.props.list.map(cur => <Number number={cur} />)}
      </div>
    )
  }
}

export default NumberList;