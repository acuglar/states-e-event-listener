import { Component } from 'react';

class Number extends Component {
  render() {
    return (
      <div style={{ border: '1px solid green' }}>
        {this.props.number}
      </div>

    )
  }
}

export default Number;